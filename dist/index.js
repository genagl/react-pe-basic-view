function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React, { Component } from "react";
import axios from "axios";
import { menu, concatRouting } from "react-pe-layouts";
import { __ } from "react-pe-utilities";
import { LayoutIcon } from 'react-pe-useful';
import { byId } from "react-pe-layouts";
import { initArea } from "react-pe-utilities";
import Feed from "./Feed";

class BasicState extends Component {
  constructor(props) {
    super(props); // console.log(this.basic_state_data());

    _defineProperty(this, "getName", () => {
      let name = menu().filter(e => e.route === this.state.route);
      if (!name) name = "404";
      return name;
    });

    _defineProperty(this, "getIcon", () => {
      let icon = menu().filter(e => e.icon === this.state.icon);
      if (!icon) icon = "fas fa-user";
      return icon;
    });

    _defineProperty(this, "getRoute", () => this.props.route);

    _defineProperty(this, "myPanelHtml", route => {
      if (route && route.panel) return /*#__PURE__*/React.createElement("div", {
        dangerouslySetInnerHTML: {
          __html: route.panel
        }
      });
      return this.state.panelHtml;
    });

    _defineProperty(this, "myState", route => {
      if (route && route.html) return /*#__PURE__*/React.createElement("div", {
        dangerouslySetInnerHTML: {
          __html: route.html
        }
      });

      if (route && route.html_sourse) {
        axios.get(route.html_sourse).then(response => {
          this.setState({
            html: /*#__PURE__*/React.createElement("div", {
              dangerouslySetInnerHTML: {
                __html: response.data
              }
            })
          });
        });
        return "";
      }

      if (route && route.data_type) {
        return ""; // this.get_data( route.data_type);
      }

      return "";
    });

    _defineProperty(this, "layout", () => {
      const name = concatRouting().filter(e => e.route == this.getRoute());
      return name.length > 0 ? name[0] : this.state.route;
    });

    const newStyle = byId(this.props.style_id);
    const isLeft = typeof this.props.is_left !== "undefined" ? this.props.is_left : null; // console.log( newStyle );

    if (newStyle || isLeft && this.props.onChangeStyle) this.props.onChangeStyle({
      fluid: 1,
      style: newStyle.url,
      isLeft
    });
    this.state = { ...this.basic_state_data(),
      html: "",
      route: {
        variables: this.props.variables ? this.props.variables : {},
        icon: this.props.icon ? this.props.icon : "usercor-light",
        title: this.props.title ? this.props.title : this.getTitle(),
        html: this.props.html ? this.props.html : "",
        panel: this.props.panel ? this.props.panel : "",
        html_sourse: this.props.html_sourse ? this.props.html_sourse : "",
        data_type: this.props.data_type ? this.props.data_type : ""
      },
      description: "",
      panelHtml: ""
    };
  }

  componentDidMount() {
    this.setState({
      route: this.layout(),
      panelHtml: this.myPanelHtml(this.layout()),
      html: this.myState(this.layout())
    });
    this.stateDidMount();
  }

  stateDidMount() {}

  componentWillReceiveProps(nextProps) {
    if (nextProps.html || nextProps.icon || nextProps.title || nextProps.html_sourse || nextProps.data_type) {
      const {
        route
      } = this.state; // console.log(nextProps);

      this.setState({
        html: this.myState(nextProps),
        panelHtml: this.myPanelHtml(nextProps),
        route: { ...route,
          icon: nextProps.icon,
          title: nextProps.title,
          html: nextProps.html,
          html_sourse: nextProps.html_sourse,
          panel: nextProps.panel,
          data_type: nextProps.data_type
        }
      });
    }
  }

  alternateRender() {
    return null;
  }

  addRender() {
    return null;
  }

  beforeRender() {
    return null;
  }

  render() {
    const alt = this.alternateRender();
    if (alt) return alt;
    this.beforeRender();
    return /*#__PURE__*/React.createElement("div", {
      className: "layout-state"
    }, this.renderTitle(), this.addRender(), this.state.html);
  }

  renderTitle() {
    return /*#__PURE__*/React.createElement("div", {
      className: "layout-state-head"
    }, /*#__PURE__*/React.createElement(LayoutIcon, {
      isSVG: true,
      src: this.state.route ? this.state.route.icon : "",
      className: "layout-state-logo "
    }), /*#__PURE__*/React.createElement("div", {
      className: "layout-state-title"
    }, __(this.state.route ? this.state.route.title : "")), /*#__PURE__*/React.createElement("div", {
      className: "layout-state-heaader"
    }, initArea("layout_heaader_panel", this)));
  }

  rightPanel() {
    return this.state.panelHtml;
  }

  getTitle() {}

  basic_state_data() {
    return {};
  }

}

export default BasicState;
export { Feed };